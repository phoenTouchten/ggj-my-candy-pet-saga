﻿using UnityEngine;
using System.Collections;

public class DummyCharacter : MonoBehaviour {

	public int choice;
	public GameObject[] choices;
	public GameObject networkObj;

	private int inactive;

	// Use this for initialization
	void Start () {
		networkObj = GameObject.Find ("NetworkText");
	
	}

	void OnEnable()
	{
		choices[0].GetComponent<tk2dUIItem>().OnClickUIItem += OnChoiceTapped;
		choices[1].GetComponent<tk2dUIItem>().OnClickUIItem += OnChoiceTapped;
		choices[2].GetComponent<tk2dUIItem>().OnClickUIItem += OnChoiceTapped;

	}

	void OnDisable()
	{
		choices[0].GetComponent<tk2dUIItem>().OnClickUIItem -= OnChoiceTapped;
		choices[1].GetComponent<tk2dUIItem>().OnClickUIItem -= OnChoiceTapped;
		choices[2].GetComponent<tk2dUIItem>().OnClickUIItem -= OnChoiceTapped;

	}

	void OnChoiceTapped(tk2dUIItem item)
	{
		int choice = item.GetComponent<Choice>().index;

		if (networkView.isMine)
		{
			RespondChoice (choice);
		}
	}
	
	[RPC]
	void RespondChoice(int index) {

		choice = index;

		for (int i= 0; i < 3; i++) 
		{
			if (!networkView.isMine) {
				choices[i].SetActive(false);
				inactive = index;
			}
			else {
				if (index != i)
				{
					choices[i].SetActive(false);
				}
			}

		}

		if (networkView.isMine) {
			networkView.RPC ("RespondChoice", RPCMode.OthersBuffered, index);
		}
	}

	void OnChoice(int index) 
	{
		choices[inactive].SetActive(true);
	}
	
}
