﻿using UnityEngine;
using System.Collections;

public class InteractableObject : MonoBehaviour {

	// self reference
	private Collider myCollider;
	private Transform myTransform;
	private Renderer myRenderer;

	private Material defaultMaterial;
	public Material activeMaterial;


	// stats
	private bool _isInteractable;
	public bool IsInteractable {
		get {
			return _isInteractable;
		}
	}

	#region Init
	private void Awake () {
		SetReferences();
	}

	private void SetReferences () {
		myCollider = collider;
		myCollider.isTrigger = true;

		myRenderer = renderer;
		defaultMaterial = myRenderer.material;
	}

	private void Start () {
		_isInteractable = false;
	}
	#endregion


	#region Public
	public virtual void Use () {
		gameObject.SetActive(false);
	}
	#endregion


	#region Event
	private void OnTriggerEnter (Collider collider) {
		switch (collider.tag) {
		case GameController.tagPlayer :
			_isInteractable = true;
			break;
		}
	}

	private void OnTriggerExit (Collider collider) {
		switch (collider.tag) {
		case GameController.tagPlayer :
			_isInteractable = false;
			break;
		}
	}
	#endregion


	#region CORE
	private void Update () {
		LoadInteractionUI();
	}

	private void LoadInteractionUI () {
		if (_isInteractable) {
			myRenderer.material = activeMaterial;
		}
		else {
			myRenderer.material = defaultMaterial;
		}
	}
	#endregion
}
