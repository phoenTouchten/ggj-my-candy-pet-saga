﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PetIdle : FSMState {
	
	[System.NonSerialized]
	public Pet pet;

	private bool isInputDetected;

	#region FSM
	public override void OnEnter ()
	{
		GameObject networkObj = GameObject.Find("MainNetworkObj");
		networkObj.networkView.RPC("ChangeState", RPCMode.All, 1);
		EnableListener();
		EventsManager.OnPetStateChanged(pet.CurrentState);
		isInputDetected = false;
	}
	
	public override void OnExit ()
	{
		DisableListener();
	}
	
	public override void OnUpdate ()
	{
		if (!isInputDetected) {
			PoolGameInput();
		}
	}
	#endregion


	#region Game Input
	private void PoolGameInput () {
		if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) ||
		    Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.W)) {
			isInputDetected = true;
			pet.GoToState(pet.s_petMove);
		}
	}
	#endregion

	
	#region Listener
	private void EnableListener () {
		EventsManager.onGamePadHoldEvent += OnHold;
		EventsManager.onGamePadDownEvent += OnHold;
	}

	void OnHold (tk2dButton source)
	{
		if (!isInputDetected) {
			isInputDetected = true;
			pet.GoToState(pet.s_petMove);
		}
	}
	
	private void DisableListener () {
		EventsManager.onGamePadHoldEvent -= OnHold;
		EventsManager.onGamePadDownEvent -= OnHold;
	}
	#endregion
}
