﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PetEat : FSMState {

	[System.NonSerialized]
	public Pet pet;
	
	#region FSM
	public override void OnEnter ()
	{
		EnableListener();
		EventsManager.OnPetStateChanged(pet.CurrentState);
	}
	
	public override void OnExit ()
	{
		DisableListener();
	}
	
	public override void OnUpdate ()
	{
		
	}
	#endregion
	
	
	#region Listener
	private void EnableListener () {
		
	}
	
	private void DisableListener () {
		
	}
	#endregion
}
