﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PetStart : FSMState {

	[System.NonSerialized]
	public Pet pet;

	#region FSM
	public override void OnEnter ()
	{
		GameObject networkObj = GameObject.Find("MainNetworkObj");
		networkObj.networkView.RPC("ChangeState",  RPCMode.All, 0);
		EnableListener();
		EventsManager.OnPetStateChanged(pet.CurrentState);
		pet.GoToState(pet.s_petIdle);
	}

	public override void OnExit ()
	{
		DisableListener();
	}

	public override void OnUpdate ()
	{

	}
	#endregion

	#region Listener
	private void EnableListener () {

	}

	private void DisableListener () {

	}
	#endregion
}
