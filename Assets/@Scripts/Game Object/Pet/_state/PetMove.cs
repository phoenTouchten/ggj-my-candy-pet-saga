﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PetMove : FSMState {

	[System.NonSerialized]
	public Pet pet;
	
	private const float movementSpeed = 15;
	private const float rotationSpeed = 10;


	#region FSM
	public override void OnEnter ()
	{
		GameObject networkObj = GameObject.Find("MainNetworkObj");
		networkObj.networkView.RPC("ChangeState",  RPCMode.All, 2);
		EnableListener();
		EventsManager.OnPetStateChanged(pet.CurrentState);
	}
	
	public override void OnExit ()
	{
		DisableListener();
	}
	
//	public override void OnUpdate ()
//	{
//		Move ();
//	}
	#endregion


	#region Movement Core
	private void Move () {
		if (Input.GetKey(KeyCode.W)) {
			pet.transform.localPosition += pet.transform.forward * movementSpeed * Time.deltaTime;
		}

		else if (Input.GetKey(KeyCode.S)) {
			pet.transform.localPosition -= pet.transform.forward * movementSpeed * Time.deltaTime;
		}


		else if (Input.GetKey(KeyCode.A)) {
			pet.transform.Rotate(Vector3.up * -rotationSpeed * Time.deltaTime);
		}

		else if (Input.GetKey(KeyCode.D)) {
			pet.transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
		}

		else {
			pet.GoToState(pet.s_petIdle);
		}
	}
	#endregion


	#region Listener
	private void EnableListener () {
		EventsManager.onGamePadHoldEvent += OnHold;
		EventsManager.onGamePadDownEvent += OnHold;
		EventsManager.onGamePadUpEvent += OnUp;
	}
	
	private void DisableListener () {
		EventsManager.onGamePadHoldEvent += OnHold;
		EventsManager.onGamePadDownEvent += OnHold;
		EventsManager.onGamePadUpEvent -= OnUp;
	}

	void OnHold (tk2dButton source)
	{
		switch (source.name) {
		case "Atas" :
			pet.transform.localPosition += pet.transform.forward * movementSpeed * Time.deltaTime;
			break;
		case "Bawah" :
			pet.transform.localPosition -= pet.transform.forward * movementSpeed * Time.deltaTime;
			break;
		case "Kiri" :
			pet.transform.Rotate(Vector3.up * -rotationSpeed * Time.deltaTime);
		break;
		case "Kanan" :
			pet.transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
			break;
		}
	}

	void OnUp (tk2dButton source) {
		pet.GoToState(pet.s_petIdle);
	}
	#endregion
}
