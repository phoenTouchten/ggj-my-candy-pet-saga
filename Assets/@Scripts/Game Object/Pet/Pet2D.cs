﻿using UnityEngine;
using System.Collections;

public class Pet2D : MonoBehaviour {

	public GameObject inviteWindow;
	public tk2dUIItem inviteButton;
	private int inviteType;

	public GameObject networkObj;

	// 2D sprite
	private tk2dSpriteAnimator petSprite;
	private const string petSpritePath = "/2D view/Level/Pet Sprite";

	// Animation Clip Name
	private const string sleepingAnimation = "sleeping";
	private const string movingAnimation = "moving";
	private const string idlingAnimation = "idling";
	private const string eatingAnimation = "eating";
	private const string interactingAnimation = "interacting";

	#region Init
	private void Awake () {
		SetReferences();
	}

	void Start() {
		networkObj = GameObject.Find("MainNetworkObj");
		inviteWindow.SetActive(false);
		inviteButton.OnClick += ReceiveInvite;
	}
	
	private void SetReferences () {
		petSprite = GameObject.Find(petSpritePath).GetComponent<tk2dSpriteAnimator>();
	}

	private void OnEnable () {
		EnableListener();
	}

	private void OnDisable () {
		DisableListener();
	}
	#endregion

	#region Listener
	private void EnableListener () {
		NetworkEventsManager.onNetworkChangeState += OnPetStateChanged;
		NetworkEventsManager.onNetworkSendInvitation += OnInvite;
		EventsManager.onPetStateChangedEvent += OnPetStateChanged;
	}


	private void DisableListener () {
		NetworkEventsManager.onNetworkChangeState -= OnPetStateChanged;
		NetworkEventsManager.onNetworkSendInvitation -= OnInvite;
		EventsManager.onPetStateChangedEvent -= OnPetStateChanged;
	}
	#endregion

	#region CORE
	private void OnInvite(int type)
	{
		inviteType = type;
		inviteWindow.SetActive(true);

	}

	void ReceiveInvite()
	{
		string scene = "";
		if (inviteType == 0)
			scene = "GGJnetworkShMUP";
		else
			scene = "GGJnetworkWhackAMole";

		networkObj.GetComponent<NetworkView>().RPC ("ChangeScene", RPCMode.All, scene);
	}

	private void OnPetStateChanged (int state)
	{

		if (state == 0)
			petSprite.Play (eatingAnimation);

		if (state == 1)
			petSprite.Play(idlingAnimation);

		if (state == 2)
			petSprite.Play (movingAnimation);

		if (state == 3)
			petSprite.Play (idlingAnimation);
	}
	private void OnPetStateChanged (FSMState petState)
	{
		if (petState is PetEat) {
			petSprite.Play(eatingAnimation);
		}	

		if (petState is PetMove) {
			petSprite.Play(movingAnimation);
		}

		if (petState is PetIdle) {
			petSprite.Play(idlingAnimation);
		}
		
		if (petState is PetStart) {
			petSprite.Play(idlingAnimation);
		}
	}
	#endregion
}
