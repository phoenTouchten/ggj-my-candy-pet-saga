﻿using UnityEngine;
using System.Collections;

public class Pet : FSMSystem {

	public GameObject networkObj;


	// state machine
	[HideInInspector] public PetStart s_petStart;
	[HideInInspector] public PetMove s_petMove;
	[HideInInspector] public PetIdle s_petIdle;
	[HideInInspector] public PetEat s_petEat;


	// stats
	private const float _maxHunger = 10;
	private float _currentHunger;

	private const float _maxHappiness = 10;
	private float _currentHappiness;

	private const float _maxHygene = 10;
	private float _currentHygene;

	private const float _maxEnergy = 10;
	private float _currentEnergy;


	private static Pet _instance;
	public static Pet Instance {
		get {
			return _instance;
		}
	}


	#region INIT
	private void Awake () {
		networkObj = GameObject.Find("MainNetworkObj");
		_instance = this;
		InitFSM();
	}

	private void OnEnable (){
//		StartFSM();
	}

	private void OnDisable () {
		CurrentState.OnExit();
	}
	#endregion

	#region State Machine
	private void InitFSM () {
		AddState(s_petStart);
		AddState(s_petIdle);
		AddState(s_petMove);
		AddState(s_petEat);

		s_petStart.pet = this;
		s_petIdle.pet = this;
		s_petMove.pet = this;
		s_petEat.pet = this;
	}

	public void StartFSM () {
		CurrentState.OnEnter();
	}
	#endregion


	#region Collider
	private void OnTriggerEnter (Collider collider) {
		EventsManager.OnPetEnterGameArea(collider);
		int inviteType = 0;

		if(collider.name == "AreaBath")
		{
			inviteType = 0;
			networkObj.networkView.RPC ("SendInvitation", RPCMode.Others, inviteType);
		}
		else if (collider.name == "AreaFood")
		{
			inviteType = 1;
			networkObj.networkView.RPC ("SendInvitation", RPCMode.Others, inviteType);
		}


		Debug.Log("Enter Game Area");
	}
	#endregion
}
