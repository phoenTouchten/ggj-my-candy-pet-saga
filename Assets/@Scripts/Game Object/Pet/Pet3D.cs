﻿using UnityEngine;
using System.Collections;

public class Pet3D : MonoBehaviour {
	
	// cache references
	private Transform myTransform;

	// stats
	private const float speed = 10;
	private bool isInteractionAvailable;
	private InteractableObject interactableObject;


	#region Init
	private void Awake () {
		SetReferences();
	}

	private void SetReferences () {
		myTransform = transform;
	}

	private void OnEnable() {

	}

	private void OnDisable () {

	}
	#endregion


	#region Update
	private void Update () {
		UpdateMovement();
		UpdateInteractionInput();
		UpdatePetState();
	}
	#endregion


	#region Event
	private void OnTriggerStay (Collider collider) {
		switch (collider.tag) {
		case GameController.tagInteractable :
			isInteractionAvailable = true;
			interactableObject = collider.GetComponent<InteractableObject>();
			break;
		}
	}

	private void OnTriggerExit (Collider collider) {
		switch (collider.tag) {
		case GameController.tagInteractable :
			isInteractionAvailable = false;
			interactableObject = null;
			break;
		}
	}
	#endregion


	#region CORE
	private void UpdateMovement () {
		if (Input.GetKey (KeyCode.S)) {
			Move (Vector3.back);
		} 

		if (Input.GetKey (KeyCode.A)) {
			Move (Vector3.left);
		}

		if (Input.GetKey (KeyCode.D)) {
			Move (Vector3.right);
		}

		if (Input.GetKey (KeyCode.W)) {
			Move (Vector3.forward);
		}
	}

	private void UpdateInteractionInput () {
		if (isInteractionAvailable) {
			if (Input.GetKey(KeyCode.Space)) {
				interactableObject.Use();
				GotoInteractingState();
			}
		}
	}

	private void UpdatePetState () {
		if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || 
		    Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.W)) {
			GotoMoveState();
		} 
		else {
			GotoIdleState();
		}
	}

	private void GotoMoveState () {
		if (Pet.Instance.CurrentState.GetType() != typeof(PetMove)) {
			Pet.Instance.GoToState(Pet.Instance.s_petMove);
		}
	}

	private void GotoIdleState () {
		if (Pet.Instance.CurrentState.GetType() != typeof(PetIdle)) {
			Pet.Instance.GoToState(Pet.Instance.s_petIdle);
		}
	}

	private void GotoInteractingState () {
		if (Pet.Instance.CurrentState.GetType() != typeof(PetEat)) {
			Pet.Instance.GoToState(Pet.Instance.s_petEat);
		}
	}
	#endregion


	#region Utility
	private void Move (Vector3 direction) {
		myTransform.localPosition += direction * speed * Time.deltaTime;
	}
	#endregion
}
