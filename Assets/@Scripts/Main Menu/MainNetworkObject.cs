//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

public class MainNetworkObject : MonoBehaviour 
{
	void Start()
	{
		DontDestroyOnLoad(this);
	}

	[RPC]
	public void RespondJoin() {
		
		Debug.Log ("RespondingJoin");
		//assign the player and pet
		NetworkEventsManager.OnNetworkEstablished();
		
		//		if (networkView.isMine)
		//			networkView.RPC ("RespondJoin", RPCMode.OthersBuffered, null);
	}
	
	[RPC]
	public void ChangeScene(string name) {
		Debug.Log ("Changing Scene..");
		
		Application.LoadLevel(name);
	}

	[RPC]
	public void StartSEU() {
		Debug.Log ("Starting ShootEmUp..");
		
		NetworkEventsManager.OnNetworkSEUStart();
	}

	[RPC]
	public void ChangeState(int state) { 
		Debug.Log ("Changing state");

		NetworkEventsManager.OnChangeState(state);
	}

	[RPC]
	public void SendInvitation(int type) {
		Debug.Log ("Sending invitation");

		NetworkEventsManager.OnNetworkSendInvitation(type);
	}

	[RPC]
	public void TapInput(int index) {
		Debug.Log ("Input Tap : " + index);

		NetworkEventsManager.OnReceiveChoice(index);

		if (networkView.isMine)
			networkView.RPC ("TapInput", RPCMode.OthersBuffered, index);
	}
}

