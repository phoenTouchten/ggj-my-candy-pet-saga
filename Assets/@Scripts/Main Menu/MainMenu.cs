﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public tk2dUIItem startButton, playerButton, petButton;
	public GameObject playerRoomPrefab;
	public GameObject networkObj;
	public tk2dTextMesh textStatus;
	
	private const string typeName = "UniqueGameName";
	private const string gameName = "RoomName";
	private HostData[] hostList;
	
	void Start()
	{
		playerButton.gameObject.SetActive(false);
		petButton.gameObject.SetActive(false);

		startButton.GetComponent<tk2dUIItem>().OnClick += OnStartPress;
		playerButton.GetComponent<tk2dUIItem>().OnClick += OnPlayerPress;
		petButton.GetComponent<tk2dUIItem>().OnClick += OnPetPress;
	}
	void OnEnable()
	{
		NetworkEventsManager.onRoomCreated += HandleonRoomCreated;
		NetworkEventsManager.onNetworkEstablished += HandleonNetworkEstablished;
	}

	void OnDisable()
	{
		NetworkEventsManager.onRoomCreated -= HandleonRoomCreated;
		NetworkEventsManager.onNetworkEstablished -= HandleonNetworkEstablished;
	}

	void OnStartPress()
	{
		startButton.gameObject.SetActive(false);
		playerButton.gameObject.SetActive(true);
		petButton.gameObject.SetActive(true);
	}

	void OnPlayerPress()
	{
		petButton.gameObject.SetActive(false);
		StartServer();
	}
	
	void HandleonRoomCreated ()
	{
		textStatus.text = "Waiting for pet who wants to play with you...";
		textStatus.Commit();
	}

	void OnPetPress()
	{
		playerButton.gameObject.SetActive(false);

		textStatus.text = "Searching for owner who wants to play with you...";
		textStatus.Commit();

		RefreshHostList();

		StartCoroutine(ShowList());
	}

	IEnumerator ShowList()
	{
		yield return new WaitForSeconds(2.0f);

		if (hostList != null)
		{
			textStatus.text = "Owners found";
			textStatus.Commit();

			for (int i = 0; i < hostList.Length; i++)
			{
				GameObject playerRoom = (GameObject)Instantiate(playerRoomPrefab, new Vector2(100, 400 + i * 80), Quaternion.identity);
				playerRoom.GetComponent<tk2dUIItem>().OnClickUIItem += JoinRoom;

			}
		}
	}

	void JoinRoom(tk2dUIItem item)
	{
		item.gameObject.SetActive(false);
		JoinServer(hostList[hostList.Length - 1]);
	}

	#region Init
	public void StartServer()
	{
		Network.InitializeServer(4, 25000, !Network.HavePublicAddress());
		MasterServer.RegisterHost(typeName, gameName);
	}
	
	public void OnServerInitialized()
	{
		Debug.Log("Server Initialized");
		NetworkEventsManager.OnRoomCreated();
		
	}
	#endregion
	
	#region Network Event
	public void RefreshHostList()
	{
		MasterServer.RequestHostList(typeName);
	}
	
	void OnMasterServerEvent(MasterServerEvent msEvent)
	{
		if (msEvent == MasterServerEvent.HostListReceived)
			hostList = MasterServer.PollHostList();
	}
	
	public void JoinServer(HostData hostData)
	{
		Debug.Log ("Joining server");
		Network.Connect(hostData);
	}
	
	void OnConnectedToServer()
	{
		Debug.Log("Server Joined");
		networkObj.GetComponent<NetworkView>().RPC ("RespondJoin", RPCMode.All, null);
	}
	#endregion

	
	void HandleonNetworkEstablished ()
	{
		textStatus.text = "Found a match!";
		textStatus.Commit();

		networkObj.GetComponent<NetworkView>().RPC ("ChangeScene", RPCMode.All, "GGJgame");
	}


}
