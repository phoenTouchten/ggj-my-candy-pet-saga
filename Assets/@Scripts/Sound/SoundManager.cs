﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public const string musicGame = "musicGame";
	public const string fxDamaged  = "sfxDamaged";
	public const string fxJump = "sfxJump";
	public const string fxKlik = "sfxKlik";
	public const string fxPickup = "sfxPickup";
	public const string fxShoot = "sfxShoot";
	public const string fxSmash = "sfxSmash";
	
}
