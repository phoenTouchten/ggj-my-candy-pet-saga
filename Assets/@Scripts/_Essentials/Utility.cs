﻿using UnityEngine;
using System.Collections;

public class Utility {

	public static void ChangeTextMesh (tk2dTextMesh textMesh, string message) {
		textMesh.text = message;
		textMesh.Commit();
	}

	public static IEnumerator MoveTo (Transform objectToMove, Vector3 targetPosition, float speed, System.Action onCompleteDelegate) {
		float t = 0f;
		Vector3 startingPosition = objectToMove.position;

		while(t < 1)
		{
			objectToMove.position = Vector3.Lerp(startingPosition, targetPosition, t);
			t += Time.deltaTime * speed;
			yield return null;
		}
		if(onCompleteDelegate != null)
			onCompleteDelegate();
	}

	public static IEnumerator MoveTo (Transform objectToMove, Vector3 targetPosition, float speed, System.Action<Vector3> onCompleteDelegate, Vector3 parameter) {
		float t = 0f;
		Vector3 startingPosition = objectToMove.position;
		
		while(t < 1)
		{
			objectToMove.position = Vector3.Lerp(startingPosition, targetPosition, t);
			t += Time.deltaTime * speed;
			yield return null;
		}
		if(onCompleteDelegate != null)
			onCompleteDelegate(parameter);
	}
}