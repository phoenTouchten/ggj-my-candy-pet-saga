﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour {

	// FIXME: Used Scene Name
	public enum SceneName {
		Init,
		Login,
		Tamagotchi,
		Labyrinth,
		Driving,
		Quiz
	}

	private SceneName _currentScene;
	private SceneName _lastScene;

	private void GotoNextScene (SceneName nextScene) {
		Application.LoadLevel(nextScene.ToString());
	}

	private void GotoPreviousScene () {
		GotoNextScene(_lastScene);
	}
}
