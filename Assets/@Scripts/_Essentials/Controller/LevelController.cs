﻿using UnityEngine;
using System.Collections;

public class LevelController : MonoBehaviour {

	public enum LevelState {
		Start,
		Play,
		Stop,
		End
	}

	private LevelState _state;

	#region Init
	protected void Awake () {
		_state = LevelState.Start;
	}

	protected void Start () {
//		StartLevel();
		StartPet();
	}

	protected void OnEnable () {
		EnableListener();
	}

	protected void OnDisable () {
		DisableListener();
	}
	#endregion
	

	#region Listener
	protected void EnableListener () {

	}

	protected void DisableListener () {

	}
	#endregion

	
	#region CORE
	private void StartLevel () {
		SetPlayerView();
	}

	private void SetPlayerView () {
		PlayerViewController.Instance.SetGameMainView(GameController.Instance.playerType);
	}

	private void StartPet () {
		Pet.Instance.StartFSM();
	}
	#endregion
}
