﻿using UnityEngine;
using System.Collections;

public class PlayerViewController : SingletonMonoBehaviour<PlayerViewController> {

	private const string viewMasterPath = "2D view";
	private const string viewPetPath = "3D view";
	private GameObject viewMaster;
	private GameObject viewPet;


	#region INIT
	private void Awake () {
		SetReferences();
	}

	private void SetReferences () {
		viewMaster = GameObject.Find(viewMasterPath);
		viewPet = GameObject.Find(viewPetPath);
	}
	#endregion


	#region CORE
	public void SetGameMainView (PlayerType playerType) {
		switch (playerType) {
		case PlayerType.Master :
			viewMaster.SetActive(true);
			viewPet.SetActive(false);
			break;
		case PlayerType.Pet :
			viewMaster.SetActive(false);
			viewPet.SetActive(true);
			break;
		}
	}
	#endregion
}
