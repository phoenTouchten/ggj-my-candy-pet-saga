﻿using UnityEngine;
using System.Collections;

public class GameController : SingletonMonoBehaviour<GameController> {

	public enum GameState {
		Initializing,
		Start,
		Play,
		Pause,
		End
	}

	public const string tagInteractable = "interactable";
	public const string tagPlayer = "Player";
	public const string tagPet = "pet";
	public const string tagArea = "area";
	public const string tagPetCamera = "petCamera";


	public PlayerType playerType;

	#region Init
	private void Awake () {

	}

	private void Start () {

	}

	private void OnEnable () {
		EnableListener();
	}

	private void OnDisable () {
		DisableListener();
	}
	#endregion


	#region Listener
	private void EnableListener () {
		EventsManager.onPlayerTypeChangedEvent += OnPlayerTypeChanged;
	}


	private void DisableListener () {
		EventsManager.onPlayerTypeChangedEvent -= OnPlayerTypeChanged;
	}
	#endregion


	#region CORE
	private void OnPlayerTypeChanged (PlayerType playerType) {
		this.playerType = playerType;
		PlayerViewController.Instance.SetGameMainView(this.playerType);
	}
	#endregion
}

public enum PlayerType {
	Master,
	Pet
}