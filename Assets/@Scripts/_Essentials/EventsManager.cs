﻿using UnityEngine;
using System.Collections;

public class EventsManager {

	#region GAME Event
	// -----------------------------------
	public delegate void GameStateHandler (GameController.GameState gameState);
	public static event GameStateHandler onGameStateChangedEvent;

	public static void OnGameStateChanged (GameController.GameState gameState) {
		if (onGameStateChangedEvent != null)
			onGameStateChangedEvent(gameState);
	}

	// -----------------------------------
	public delegate void PlayerTypeHandler (PlayerType playerType);
	public static event PlayerTypeHandler onPlayerTypeChangedEvent;

	public static void OnPlayerTypeChanged (PlayerType playerType) {
		if (onPlayerTypeChangedEvent != null)
			onPlayerTypeChangedEvent(playerType);
	}
	#endregion


	#region Pet Event
	public delegate void PetStateHandler (FSMState petState);
	public static event PetStateHandler onPetStateChangedEvent;

	public static void OnPetStateChanged (FSMState petState) {
		if (onPetStateChangedEvent != null)
			onPetStateChangedEvent(petState);
	}

	public delegate void OnPetTriggerHandler (Collider collider);
	public static event OnPetTriggerHandler onPetEnterGameAreaEvent;

	public static void OnPetEnterGameArea (Collider collider) {
		if (onPetEnterGameAreaEvent != null) {
			onPetEnterGameAreaEvent(collider);
		}
	}
	#endregion


	#region Object
	public delegate void ObjectInteractionHandler (InteractableObject interactableObject);
	public static event ObjectInteractionHandler onObjectInteractionAvailableEvent;

	public static void OnObjectInteractionAvailable (InteractableObject interactableObject) {
		if (onObjectInteractionAvailableEvent != null)
			onObjectInteractionAvailableEvent(interactableObject);
	}


	#endregion


	#region Whack A Mole - Food
	public delegate void FoodBoxHandler (FoodBox foodbox);
	public static event FoodBoxHandler onFoodboxTriggeredEvent;

	public static void OnFoodboxTriggered (FoodBox foodbox) {
		if (onFoodboxTriggeredEvent != null)
			onFoodboxTriggeredEvent(foodbox);
	}
	#endregion



	#region GamePad
	public delegate void GamepadHandler (tk2dButton source);
	public static event GamepadHandler onGamePadUpEvent;
	public static event GamepadHandler onGamePadDownEvent;
	public static event GamepadHandler onGamePadHoldEvent;

	public static void OnUp(tk2dButton source) {
		if (onGamePadUpEvent != null)
			onGamePadUpEvent(source);
	}

	public static void OnDown (tk2dButton source) {
		if (onGamePadDownEvent != null)
			onGamePadDownEvent(source);
	}

	public static void OnHold (tk2dButton source) {
		if (onGamePadHoldEvent != null)
			onGamePadHoldEvent(source);
	}
	#endregion
}