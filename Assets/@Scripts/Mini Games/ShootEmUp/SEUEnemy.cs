using UnityEngine;
using System.Collections;

public class SEUEnemy : MonoBehaviour
{
	public float maxSpeed;
	public int health;
	
	void FixedUpdate()
	{
		Vector3 newpos = new Vector3( this.transform.position.x, this.transform.position.y - maxSpeed * Time.fixedDeltaTime);
		this.transform.position = newpos;
		
		if (this.transform.position.y < -3000)
			Destroy(this.gameObject);
	}

	void OnTriggerEnter(Collider others)
	{
		health--;
		if (health <= 0)
		{
			Destroy(this.gameObject);
		}
	}
}