using UnityEngine;
using System.Collections;

public class SEUEnemySpawner : MonoBehaviour {
	public GameObject enemyPrefab;
	
	void Start()
	{

	}

	void OnEnable()
	{
		NetworkEventsManager.onNetworkSEUStart += OnEstablish;
	}

	void OnDisable()
	{
		NetworkEventsManager.onNetworkSEUStart -= OnEstablish;
	}

	void OnEstablish()
	{
		StartCoroutine(SpawnEnemy());
	}

	IEnumerator SpawnEnemy()
	{
		bool isOK = false;
		while (!isOK) {
			Network.Instantiate(enemyPrefab, new Vector3(Random.Range (-500, 500), this.transform.position.y), Quaternion.identity, 0);
			yield return new WaitForSeconds(Random.Range(0.5f, 2f));
		
		}
	}
	
}