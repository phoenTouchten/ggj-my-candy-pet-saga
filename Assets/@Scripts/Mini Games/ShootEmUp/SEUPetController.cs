using UnityEngine;
using System.Collections;

public class SEUPetController : MonoBehaviour
{
	public float maxSpeed;
	public float velocity;
	public int direction;

	public GameObject ammoPrefab;

	private float lastSynchronizationTime = 0f;
	private float syncDelay = 0f;
	private float syncTime = 0f;
	private Vector3 syncStartPosition = Vector3.zero;
	private Vector3 syncEndPosition = Vector3.zero;
	
	void Start()
	{
		direction = 0;
		this.GetComponent<tk2dUIItem>().OnClick += HandleOnClick;
	}

	void HandleOnClick ()
	{
		if (!networkView.isMine)
			Network.Instantiate(ammoPrefab, new Vector2(this.transform.position.x, this.transform.position.y + 150), Quaternion.identity, 0);
	}

	void OnEnable () {
		EventsManager.onGamePadHoldEvent += OnPressed;
		EventsManager.onGamePadDownEvent += OnPressed;
		EventsManager.onGamePadUpEvent += OnUp;
	}

	void OnPressed (tk2dButton source)
	{
		switch (source.name) {
		case "Kiri" :
			direction = -1;
			break;
		case "Kanan" :
			direction = 1;
			break;
		}
		velocity = maxSpeed * direction;
	}

	void OnUp (tk2dButton source) {
		direction = 0;
	}

	void OnDisable () {
		EventsManager.onGamePadHoldEvent -= OnPressed;
		EventsManager.onGamePadDownEvent -= OnPressed;
		EventsManager.onGamePadUpEvent -= OnUp;
	}

//	void Update()
//	{
////		if (networkView.isMine)
////		{
////			direction = 0;
////			if (Input.GetKey(KeyCode.LeftArrow)) {
////				direction = -1;
////			}
////			else if (Input.GetKey(KeyCode.RightArrow)) {
////				direction = 1;
////			}
////			
////		}
//		velocity = maxSpeed * direction;
//	}

	void FixedUpdate()
	{
		if (networkView.isMine) {
			Vector3 newpos = new Vector3( this.transform.position.x + velocity * Time.fixedDeltaTime, this.transform.position.y);
			this.transform.position = newpos;
		}
		else
		{
			syncTime += Time.fixedDeltaTime;
			this.transform.position = Vector3.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay);
		}
	}

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
	{
		Vector3 syncPosition = Vector2.zero;

		float syncX = 0f;
		if (stream.isWriting) {
			syncPosition = this.transform.position;
	
			stream.Serialize(ref syncPosition);
		}
		else {
			stream.Serialize(ref syncPosition);

			syncTime = 0f;
			syncDelay = Time.time - lastSynchronizationTime;
			lastSynchronizationTime = Time.time;
			
			syncStartPosition = this.transform.position;
			syncEndPosition = syncPosition;
			}
	}
}

