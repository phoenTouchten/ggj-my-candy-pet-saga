﻿using UnityEngine;
using System.Collections;

public class FoodBox : MonoBehaviour {

	// references
	private tk2dUIItem myUIitem;
	private tk2dSlicedSprite mySprite;
	private tk2dTextMesh myTextMesh;
	private tk2dSprite appleSprite;

	// stats
	private bool isActive;
	private int id;

	// pseudo-constants
	private Color activeColor = Color.white;
	private Color emptyColor = Color.red;

	private const string goodApple = "goodapple";
	private const string badApple = "badapple";


	#region Init
	private void Awake () {
		SetReferences();
	}

	private void SetReferences () {
		myUIitem = GetComponent<tk2dUIItem>();
		mySprite = GetComponent<tk2dSlicedSprite>();
		myTextMesh = GetComponentInChildren<tk2dTextMesh>();
		appleSprite = GetComponentInChildren<tk2dSprite>();
	}

	private void Start () {
		isActive = false;
		ChangeColor();
	}

	private void OnEnable () {
		EnableListener();
	}

	private void OnDisable () {
		DisableListener();
	}
	#endregion
	

	#region Listener
	private void EnableListener () {
		myUIitem.OnDownUIItem += OnUIitemDown;
	}
	
	private void DisableListener () {
		myUIitem.OnDownUIItem -= OnUIitemDown;
	}

	void OnUIitemDown (tk2dUIItem obj)
	{
		EventsManager.OnFoodboxTriggered(this);
	}
	#endregion


	#region Public Methods
	public bool IsActive {
		get { return isActive; }
		set { 
			isActive = value; 
			ChangeColor();
			ChangeAppleSprite();
		}
	}

	public int Id {
		get {
			return id;
		}
		set {
			id = value;
			Utility.ChangeTextMesh(myTextMesh, id.ToString());
		}
	}

	#endregion


	#region CORE
	private void ChangeColor() {
		switch (isActive) {
		case true :
			mySprite.color = activeColor;
			break;
		case false :
			mySprite.color = emptyColor;
			break;
		}
	}

	private void ChangeAppleSprite () {
		switch (isActive) {
		case true :
			appleSprite.SetSprite(goodApple);
			break;
		case false :
			appleSprite.SetSprite(badApple);
			break;
		}
	}
	#endregion
}
