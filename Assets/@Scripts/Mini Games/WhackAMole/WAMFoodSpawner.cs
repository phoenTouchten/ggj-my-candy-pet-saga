using UnityEngine;
using System.Collections;

public class WAMFoodSpawner : MonoBehaviour
{
	public GameObject[] spawnLocation;

	public GameObject foodPrefab;



	void OnEnable() {
		NetworkEventsManager.onReceiveChoice += Spawn;
	}

	void OnDisable() {
		NetworkEventsManager.onReceiveChoice -= Spawn;
	}

	void Spawn (int location) {
		Debug.Log ("Spawn");
		Debug.Log (spawnLocation.Length);
		GameObject.Instantiate (foodPrefab, spawnLocation[location].transform.position, Quaternion.identity);
	}


}