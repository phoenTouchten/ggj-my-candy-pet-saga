﻿using UnityEngine;
using System.Collections;

public class WAMmasterController : MonoBehaviour {

	private NetworkView networkView;

	#region Init
	private void Awake () {
		networkView = GameObject.FindObjectOfType<NetworkView>();
	}

	private void OnEnable () {
		EnableListener();
	}

	private void OnDisable () {
		DisableListener();
	}

	private void Start () {
		StartLevel(0);
	}
	#endregion


	#region Listener
	private void EnableListener () {
		NetworkEventsManager.onSendChoice += StartLevel;
		EventsManager.onFoodboxTriggeredEvent += OnFoodboxTriggered;
	}

	private void DisableListener () {
		NetworkEventsManager.onSendChoice += StartLevel;
	}
	#endregion


	#region CORE
	private void StartLevel (int index) {
		FoodBoxController.Instance.StartShuffle();
	}

	private void OnFoodboxTriggered (FoodBox foodbox)
	{
		if (foodbox.IsActive) {
			networkView.RPC(WAMNetworkObject.RPCTapInput, RPCMode.Others, foodbox.Id);
		}
	}
	#endregion
}
