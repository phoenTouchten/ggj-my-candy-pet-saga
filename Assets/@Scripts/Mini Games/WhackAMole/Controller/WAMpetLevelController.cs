﻿using UnityEngine;
using System.Collections;

public class WAMpetLevelController : MonoBehaviour {

	private WAMPetController pet;
	private Camera myCamera;
	public Transform startingArea;


	#region Init
	private void Awake () {
		SetReferences();
	}

	private void SetReferences () {
		pet = GameObject.FindGameObjectWithTag(GameController.tagPet).GetComponent<WAMPetController>();
		myCamera = GameObject.FindGameObjectWithTag(GameController.tagPetCamera).GetComponent<Camera>();
	}

	private void OnEnable () {
		EnableListener();
	}

	private void OnDisable () {
		DisableListener();
	}

	private void Start () {
		SetPetInitialPos();
	}

	private void SetPetInitialPos () {
		Vector3 startingPos = startingArea.position;
		pet.transform.position = new Vector3(startingPos.x, pet.transform.position.y, startingPos.z);
	}
	#endregion


	#region Listener
	private void EnableListener () {
		Gesture.onMouse1DownE += OnPressed;
		Gesture.onTouchDownE += OnPressed;
	}

	private void DisableListener () {
		Gesture.onMouse1DownE -= OnPressed;
		Gesture.onTouchDownE -= OnPressed;
	}
	#endregion
	

	#region Input
	void OnPressed (Vector2 pos)
	{
		Ray ray = myCamera.ScreenPointToRay(pos);
		RaycastHit hit;

		if (Physics.Raycast(ray, out hit, Mathf.Infinity)) {
			switch (hit.transform.tag) {
			case GameController.tagArea :
				MovePetToArea(hit.transform);
				break;
			}
		}
	}
	#endregion


	#region Core
	private void MovePetToArea (Transform area) {
		pet.MoveToLocation(area.position);
	}
	#endregion
}
