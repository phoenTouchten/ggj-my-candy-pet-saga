﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FoodBoxController : MonoBehaviour {

	private static FoodBoxController _instance;
	public static  FoodBoxController Instance {
		get {
			return _instance;
		}
	}

	// foodbox
	public GameObject foodboxPrefabs;
	private List<FoodBox> foodboxes;
	private const int rowSize = 4;
	private const int columnSize = 2;
	private const int rowOfsett = -450;
	private const int columnOffset = -190;
	private const int columnStep = 380;
	private const int rowStep = 300;

	// foodbox index 
	private int lastRandomIndex;
	private int randomIndex;

	// timeout
	private const float timeout = 1f;
	private float elapsedTime;


	#region Init
	private void Awake () {
		_instance = this;
		foodboxes = new List<FoodBox>();
		CreateFoodbox();
		enabled = false;
	}

	private void CreateFoodbox () {
		Vector3 position = Vector3.zero;
		int id = 0;
		for (int i = 0; i < columnSize; i++) {
			for (int j = 0; j < rowSize; j++) {
				position = new Vector3(i * columnStep + columnOffset, j * rowStep + rowOfsett ,0);
				GameObject foodboxGO = Instantiate(foodboxPrefabs) as GameObject;
				Transform foodboxTransform = foodboxGO.transform;
				foodboxTransform.parent = transform;
				foodboxTransform.localPosition = position;
				FoodBox foodbox = foodboxGO.GetComponent<FoodBox>();
				foodbox.Id = id;
				id++;
				foodboxes.Add(foodbox);
			}
		}
	}

	private void OnEnable () {
		EventsManager.onFoodboxTriggeredEvent += OnFoodboxTriggered;
	}

	private void OnDisable () {
		EventsManager.onFoodboxTriggeredEvent -= OnFoodboxTriggered;
	}
	#endregion


	#region PUBLIC
	public void StartShuffle () {
		lastRandomIndex = 0;
		ShuffleFoodbox();
		enabled = true;
	}

	public int FoodboxIndex {
		get {
			return randomIndex;
		}
	}
	#endregion


	#region Update
	private void Update () {
		UpdateTimeout();
	}

	private void UpdateTimeout () {
		elapsedTime += Time.deltaTime;
		if (elapsedTime > timeout) {
			ShuffleFoodbox();
		}
	}
	#endregion


	#region Core Methds
	private void OnFoodboxTriggered (FoodBox foodbox) {
		ShuffleFoodbox();
	}

	private void ShuffleFoodbox () {
		DeactivateAllFoodbox();
		ActivateOneRandomFoodbox();
		SetTimeout();
	}

	private void DeactivateAllFoodbox () {
		foreach (FoodBox foodbox in foodboxes) {
			foodbox.IsActive = false;
		}
	}

	private void ActivateOneRandomFoodbox () {
		do {
			randomIndex = Random.Range(0, foodboxes.Count);
		} while (randomIndex == lastRandomIndex);
		
		foodboxes[randomIndex].IsActive = true;
		lastRandomIndex = randomIndex;
	}

	private void SetTimeout () {
		elapsedTime = 0;
	}
	#endregion
}
