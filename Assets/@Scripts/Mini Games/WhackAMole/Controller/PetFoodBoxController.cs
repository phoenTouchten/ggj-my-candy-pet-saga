﻿using UnityEngine;
using System.Collections;

public class PetFoodBoxController : MonoBehaviour {

	public GameObject[] foodboxes;
	public GameObject foodPrefab;

	int index;

	#region Init
	private void Awake () {

	}

	private void OnEnable () {
		NetworkEventsManager.onReceiveChoice += Spawn;
	}

	private void OnDisable () {
		NetworkEventsManager.onReceiveChoice -= Spawn;
	}
	#endregion

	private void Update () {
		if (Input.GetKeyDown(KeyCode.A)) {
			Spawn(index);
			index++;
			if (index >= foodboxes.Length) index = 0;
		}
	}


	#region CORE
	private void Spawn (int index) {
		GameObject.Instantiate (foodPrefab, foodboxes[index].transform.position, Quaternion.identity);
	}
	#endregion
}
