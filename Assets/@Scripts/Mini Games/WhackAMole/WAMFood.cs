using UnityEngine;
using System.Collections;

public class WAMFood : MonoBehaviour
{
	private const float dropSpeed = 10;

	// reference
	private Transform myTransform;
	
	#region Init
	private void Awake () {
		myTransform = transform;
	}
	#endregion

	#region Update
	private void Update () {
		myTransform.position += Vector3.down * dropSpeed * Time.deltaTime;
	}
	#endregion

	private void OnTriggerEnter (Collider collider) {
		Destroy(gameObject);
	}
}

