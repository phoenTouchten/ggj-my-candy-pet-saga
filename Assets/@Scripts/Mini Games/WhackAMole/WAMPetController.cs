using UnityEngine;
using System.Collections;

public class WAMPetController : MonoBehaviour {

	public enum PetState {
		Idle,
		Moving
	}

	private PetState _petState;


	// reference
	private Transform myTransform;

	// constants
	private const float speed = 2;


	#region Init
	private void Awake () {
		myTransform = transform;
	}

	private void Start () {

	}
	#endregion




	#region Movement Core
	public void MoveToLocation (Vector3 position) {
		switch (_petState) {
		case PetState.Idle :
			_petState = PetState.Moving;
			MovePet(position);
			break;
		}
	}

	private void MovePet(Vector3 position) {
		MoveHorizontal(position);
	}

	private void MoveHorizontal(Vector3 position) {
		Vector3 targetPosition = new Vector3(position.x, myTransform.position.y, myTransform.position.z);
		StartCoroutine(Utility.MoveTo(myTransform, targetPosition, speed, MoveVertical, position));
	}

	private void MoveVertical (Vector3 position) {
		Vector3 targetPosition = new Vector3(myTransform.position.x, myTransform.position.y, position.z);
		StartCoroutine(Utility.MoveTo(myTransform, targetPosition, speed, OnFinishedMoving));
	}

	private void OnFinishedMoving () {
		_petState = PetState.Idle;
	}
	#endregion
}

