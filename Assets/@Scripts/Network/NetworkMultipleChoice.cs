﻿using UnityEngine;
using System.Collections;

public class NetworkMultipleChoice : MonoBehaviour {


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnChooseChoice() {
		if (networkView.isMine) {
			Debug.Log("Choosing choice");
			networkView.RPC ("RespondChoice", RPCMode.Others, 0);
		}
	}



}
