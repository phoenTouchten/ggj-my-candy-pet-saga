using UnityEngine;

public class WAMNetworkObject : MonoBehaviour
{
	public const string RPCRespondJoin = "RespondJoin";
	public const string RPCTapInput = "TapInput";

	[RPC]
	public void RespondJoin() {

		Debug.Log ("RespondingJoin");
		//assign the player and pet
		NetworkEventsManager.OnSendChoice(0);

		if (networkView.isMine)
			networkView.RPC (RPCRespondJoin, RPCMode.OthersBuffered, null);
	}

	[RPC]
	public void TapInput(int index) {
		Debug.Log ("Input Tap : " + index);

		NetworkEventsManager.OnReceiveChoice(index);

		if (networkView.isMine)
			networkView.RPC (RPCTapInput, RPCMode.OthersBuffered, index);
	}
}