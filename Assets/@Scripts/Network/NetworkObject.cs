﻿using UnityEngine;
using System.Collections;

public class NetworkObject : MonoBehaviour {

	public int choiceAmt;
	public tk2dTextMesh text;
	public tk2dUIProgressBar progressBar;

	// Use this for initialization
	void Start () {
		Debug.Log ("A network object has been instantiated.");
		choiceAmt = 0;
	}

	[RPC]
	public void RespondJoin() {
		StartCoroutine(StartCountdown());

		if (networkView.isMine) {
			networkView.RPC ("RespondJoin", RPCMode.OthersBuffered, null);
		}
	}

	IEnumerator StartCountdown()
	{
		Debug.Log ("START");
		while (progressBar.Value < 1.0f)
		{
			progressBar.Value += 0.01f;
			yield return new WaitForSeconds(0.2f);
		}

		// get the choice value of dummy character 1 and 2
		GameObject a = GameObject.Find ("DummyCharacter(Clone)");
		GameObject b = GameObject.Find ("DummyCharacter2(Clone)");

		if (a.GetComponent<DummyCharacter>().choice == b.GetComponent<DummyCharacter>().choice) {
			Debug.Log ("KAMU SEHATI!");
			RespondText(1);
		}
		else {
			Debug.Log ("KAMU TIDAK SEHATI");
			RespondText(0);
		}


	}

	[RPC]
	public void RespondText (int isMatch) {
		Debug.Log ("RespondText");

		if (isMatch == 1)
			text.text = "Kalian Sehati!";
		else
			text.text = "Kalian Tak Sehati!";

		text.Commit();

		if (networkView.isMine) {
			networkView.RPC ("RespondText", RPCMode.OthersBuffered, isMatch);
		}
	}


}
