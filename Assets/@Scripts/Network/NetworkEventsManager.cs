using UnityEngine;
using System.Collections;

public class NetworkEventsManager {

	#region delegates
	public delegate void SendChoiceHandler(int index);
	public delegate void ReceiveChoiceHandler(int index);
	public delegate void RoomCreatedHandler();
	public delegate void NetworkEstablishedHandler();
	public delegate void NetworkSEUStartHandler();
	public delegate void NetworkChangeStateHandler(int state);
	public delegate void NetworkSendInvitation(int type);

	#endregion

	#region events
	public static event SendChoiceHandler onSendChoice;
	public static event ReceiveChoiceHandler onReceiveChoice;
	public static event RoomCreatedHandler onRoomCreated;
	public static event NetworkEstablishedHandler onNetworkEstablished;
	public static event NetworkSEUStartHandler onNetworkSEUStart;
	public static event NetworkChangeStateHandler onNetworkChangeState;
	public static event NetworkSendInvitation onNetworkSendInvitation;
	#endregion

	#region handlers
	public static void OnSendChoice(int index) {
		if (onSendChoice != null)
			onSendChoice(index);
	}

	public static void OnReceiveChoice(int index) {
		if (onReceiveChoice != null)
			onReceiveChoice(index);
	}

	public static void OnRoomCreated() {
		if (onRoomCreated != null)
			onRoomCreated();
	}

	public static void OnNetworkEstablished() {
		if (onNetworkEstablished != null)
			onNetworkEstablished();
	}

	public static void OnNetworkSEUStart() {
		if (onNetworkSEUStart != null)
			onNetworkSEUStart();
	}

	public static void OnChangeState(int state) {
		if (onNetworkChangeState != null)
			onNetworkChangeState(state);
	}

	public static void OnNetworkSendInvitation(int type) {
		if (onNetworkSendInvitation != null)
			onNetworkSendInvitation(type);
	}
	#endregion
}
