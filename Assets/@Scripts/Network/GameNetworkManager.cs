using UnityEngine;
using System.Collections;

public class GameNetworkManager : MonoBehaviour
{
	public GameObject networkObj;
	public GameObject view2d, view3d;

	void Awake()
	{
		networkObj = GameObject.Find("MainNetworkObj");
		view2d = GameObject.Find ("2D view");
		view3d = GameObject.Find ("3D view");
	}

	void Start()
	{
		if (networkObj.networkView.isMine)
			view3d.SetActive(false);
		else
			view2d.SetActive(false);
	}

}