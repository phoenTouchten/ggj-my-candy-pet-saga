﻿using UnityEngine;
using System.Collections;

public class Test : MonoBehaviour {

	private void OnEnable () {
		EventsManager.onGamePadDownEvent += OnPadDown;
		EventsManager.onGamePadUpEvent += OnPadUp;
		EventsManager.onGamePadHoldEvent += OnPressed;
	}

	void OnPadUp (tk2dButton source)
	{
		Debug.Log(source.name);
	}

	void OnPadDown (tk2dButton source)
	{
		Debug.Log(source.name);
	}

	void OnPressed (tk2dButton source) {
		Debug.Log("pressed" + source.name);
	}

	private void OnDisable () {
		EventsManager.onGamePadDownEvent -= OnPadDown;
		EventsManager.onGamePadUpEvent -= OnPadUp;
		EventsManager.onGamePadHoldEvent -= OnPressed;
	}
}
