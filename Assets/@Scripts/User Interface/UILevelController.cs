﻿using UnityEngine;
using System.Collections;

public class UILevelController : MonoBehaviour {

	private tk2dUIItem changeView;
	private const string buttonChangeViewPath = "/UI/UIcamera/AnchorButton/btnChangeView"; 


	#region Init
	private void Awake () {
		SetReferences();
	}

	private void SetReferences () {
		changeView = GameObject.Find(buttonChangeViewPath).GetComponent<tk2dUIItem>();
		Debug.Log(changeView);
	}

	private void OnEnable () {
		EnableListener();
	}

	private void OnDisable () {
		DisableListener();
	}
	#endregion


	#region Listener
	private void EnableListener () {
		changeView.OnUp += OnChangeViewUp;
	}

	private void DisableListener () {
		changeView.OnUp -= OnChangeViewUp;
	}	

	private void OnChangeViewUp () {
		PlayerType playerType = PlayerType.Master;
		switch (GameController.Instance.playerType) {
		case PlayerType.Master :
			playerType = PlayerType.Pet;
			break;
		case PlayerType.Pet :
			playerType = PlayerType.Master;
			break;
		}
		EventsManager.OnPlayerTypeChanged(playerType);
	}
	#endregion
}
