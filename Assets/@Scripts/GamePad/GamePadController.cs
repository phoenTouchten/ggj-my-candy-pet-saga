﻿using UnityEngine;
using System.Collections;

public class GamePadController : MonoBehaviour {

	public tk2dButton[] gamepadButtons;

	private void OnEnable () {
		foreach (tk2dButton button in gamepadButtons) {
			button.ButtonDownEvent += OnDownButton;
			button.ButtonUpEvent += OnUpButton;
			button.ButtonAutoFireEvent += OnHoldButton;
		}
	}
	
	private void OnDisable () {
		foreach (tk2dButton button in gamepadButtons) {
			button.ButtonDownEvent -= OnDownButton;
			button.ButtonUpEvent -= OnUpButton;
			button.ButtonAutoFireEvent -= OnHoldButton;
		}
	}

	void OnDownButton (tk2dButton source)
	{
		EventsManager.OnUp(source);
	}

	void OnUpButton (tk2dButton source)
	{
		EventsManager.OnDown(source);
	}

	void OnHoldButton (tk2dButton source) {
		EventsManager.OnHold(source);
	}
}
